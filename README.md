Übung(en): Delivery (CD) mit Containern
=======================================

Einfache Web Applikation - CD Erweiterung für Container

Paketierung als Container
-------------------------

Um unsere Applikation in ein Container Image zu Paketieren, benötigen wir ein `Dockerfile`.

Das sieht wie folgt aus:

    FROM openjdk:8-jre-alpine
    COPY target/web-app-1.0-SNAPSHOT-jar-with-dependencies.jar /
    USER 1000:1000
    CMD [ "java", "-jar", "/web-app-1.0-SNAPSHOT-jar-with-dependencies.jar" ]

Als Base Image kommt die kleinste Java Umgebung, basierend auf Alpine Linux zum Einsatz.

Dann wird die Single Jar Datei ins Image kopiert.

Aus Sicherheitsgründen auf den ersten User und Gruppe gewechselt.

Und zum Schluss unsere Jar Datei ausgeführt.

Die Paketierung als Container Image erfolgt mittels `docker build`. Vorher müssen wir die Software builden:

    git clone ..../web-app
    cd web-app
    mvn clean compile assembly:single
    docker build -t my-app .

Testen der Paketierten Applikation
----------------------------------

Um die Applikation zu Testen, wird `docker run` verwendet:

    docker run -it --rm -p 8080:8080 my-app

In einem zweiten Fenster kann die Ausgabe getestet werden:

    curl localhost:8080/myapp/myresource
    





